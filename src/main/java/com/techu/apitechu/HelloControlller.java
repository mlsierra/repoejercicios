package com.techu.apitechu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControlller {

    @RequestMapping("/")
    public String index(){
        return "Hola Mundo dede API Tech UI";
    }
    @RequestMapping("/hello")
    public String hello(@RequestParam(value="name", defaultValue ="Tech U") String name ){
        return String.format("Hola %s!", name);
    }


}
